﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace GeekDay4_Dyna.Classes
{
    public class MyTCPClient
    {
        OneClient client;
        public string Nickname { get; private set; }

        public event EventHandler<string> MessageReceived
        {
            add { client.MessageReceived += value; }
            remove { client.MessageReceived -= value; }
        }
        public event EventHandler ClientClosed
        {
            add { client.ClientClosed += value; }
            remove { client.ClientClosed -= value; }
        }

        public MyTCPClient(string host, int port, string nick, string password, string udphost, string udpport)
        {
            TcpClient TC = new TcpClient();
            TC.Connect(host, port);
            client=new OneClient(TC);
            client.Send(String.Format("/LOGIN {0} {1} {2} {3}", nick, password, udphost, udpport));
            Nickname = nick;
        }

        ~MyTCPClient()
        {
            client.Close();
        }

        public void SendMsg(string line)
        {
            client.Send(line);
        }

        public void Close()
        {
            client.Close();
        }
    }
}
