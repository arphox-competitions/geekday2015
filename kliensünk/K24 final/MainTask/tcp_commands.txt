/LOGIN 	User	Password		// Login to the system (first login = registration)
/DOWN							// Continuous down movement (only from standing position)
/UP								// Continuous up movement (only from standing position)
/LEFT							// Continuous left movement (only from standing position)
/RIGHT							// Continuous right movement (only from standing position)
/REVERSE						// Reverse the continuous movement
/STOP							// Stop the continuous movement
/BOMB							// Place bomb
Commands only available during development time
/BOMBSIZE 3						// Set the size of the explosions
/BOMBNUM 5						// Set the number of bombs
/SPEED 0.8						// Set the speed
