The TCP communication is string based, and mainly textual: every TCP message has the format of [PREFIX][MESSAGE] where [PREFIX] is an Int32, stating the size of the message and [MESSAGE] is the text message (e.g. /RIGHT) that you want to send.

So, to send /RIGHT in TCP, you have to send 10 bytes:
- the first 4 bytes is the Int32 equivalent of 6 (/RIGHT is 6 letters)
- the rest 6 bytes is the letters in /RIGHT
