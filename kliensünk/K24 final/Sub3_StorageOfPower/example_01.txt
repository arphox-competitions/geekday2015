﻿<?xml version="1.0" encoding="utf-8"?>
<bomba_lista>
<bomba>
  <azonosito>
    <név>Little Boy</név>
    <id>c234giom</id>
  </azonosito>
  <tulajdonságok>
    <típus>Nuclear weapon</típus>
    <tervező>Los Alamos Laboratory</tervező>
    <gyártási_ország>USA</gyártási_ország>
    <gyártási_dátum>1945</gyártási_dátum>
    <készített_mennyiség>32</készített_mennyiség>
    <töltet>Uranium-235</töltet>
    <súly>4400 kg</súly>
    <hossz>3 m</hossz>
    <átmérő>71 cm</átmérő>
    <robbanási_hozam>63 TJ</robbanási_hozam>
    <használatban>IGEN</használatban>
  </tulajdonságok>
</bomba>
</bomba_listat>