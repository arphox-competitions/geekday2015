﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public static class Settings
    {
        public static string NickName = "ArgEx";
        public static string Password = "PASS";
        public static string SelfIP = GetLocalIPAddress();
        public static int UDP_Output_Port = 7777;

        public static string TcpIpAddress = "172.25.1.99";
        public static int TcpPort = 6666;


        public static int TimeOutSeconds = 30;

        static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    string s = ip.ToString();
                    if (s.StartsWith("172."))
                        return s;
                }
            }
            return null;
        }
    }
}
