﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    public static class Commands
    {
        public static void Left()
        {
            Send("/LEFT");         
        }

        public static void Right()
        {
            Send("/RIGHT");
        }

        public static void Up()
        {
            Send("/UP");
        }

        public static void Down()
        {
            Send("/DOWN");
        }

        public static void Reverse()
        {
            Send("/REVERSE");
        }

        public static void Stop()
        {
            Send("/STOP");
        }

        public static void Bomb()
        {
            Send("/BOMB");
        }

        static void Send(string uzenet)
        {
            UTF8Encoding utf8encoding = new UTF8Encoding();
            byte[] uzenetByteok = utf8encoding.GetBytes(uzenet);
            UInt32 hossz = (UInt32)uzenetByteok.Length;

            BinaryWriter binaryWriter = new BinaryWriter(TcpUdp.tcpStream);
            binaryWriter.Write(hossz);

            binaryWriter.Write(uzenetByteok);
        }
    }
}
