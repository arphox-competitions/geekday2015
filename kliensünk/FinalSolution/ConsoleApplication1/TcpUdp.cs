﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using karesz;

namespace ConsoleApplication1
{
    class TcpUdp
    {
        public static TcpClient tcpClient;
        public static Stream tcpStream;
        public static UdpClient udpClient;

        public TcpUdp()
        {
            CreateTCPConnectionANDLogin();
            CreateUPDConnection();
        }

        private static void CreateTCPConnectionANDLogin()
        {
            tcpClient = new TcpClient();
            tcpClient.Connect(Settings.TcpIpAddress, Settings.TcpPort);

            tcpStream = tcpClient.GetStream();

            Send(String.Format("/LOGIN {2} {3} {0} {1}", Settings.SelfIP, Settings.UDP_Output_Port, Settings.NickName, Settings.Password));
        }
        private static void CreateUPDConnection()
        {
            udpClient = new UdpClient(Settings.UDP_Output_Port);
            IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, Settings.UDP_Output_Port);

            TimeSpan TimeOutLimit = new TimeSpan(0, 0, Settings.TimeOutSeconds);

            try
            {
                while (true)
                {
                    //Console.WriteLine("Waiting for broadcast");

                    byte[] bytes = udpClient.Receive(ref ipEndPoint);

                    XMLFileProcessor.NewPacket(bytes);

                    if (XMLFileProcessor.SW.Elapsed > TimeOutLimit)
                    {
                        XMLFileProcessor.SW.Reset();
                        Reconnect();
                        Debug.WriteLine(string.Format("{0}: ForceReconnect! (Timeout = {1})", DateTime.Now, XMLFileProcessor.SW.Elapsed));
                    }

                    //Console.WriteLine("Received broadcast from {0} :\n {1}\n", ipEndPoint.ToString(), Encoding.ASCII.GetString(bytes, 0, bytes.Length));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                udpClient.Close();
            }
        }
        public static void Reconnect()
        {
            udpClient.Close();
            udpClient = new UdpClient(Settings.UDP_Output_Port);

            tcpClient.Close();
            tcpClient = new TcpClient();
            tcpClient.Connect(Settings.TcpIpAddress, Settings.TcpPort);

            tcpStream = tcpClient.GetStream();

            Send(String.Format("/LOGIN {2} {3} {0} {1}", Settings.SelfIP, Settings.UDP_Output_Port, Settings.NickName, Settings.Password));
        }

        public static void Send(string uzenet)
        {
            UTF8Encoding utf8encoding = new UTF8Encoding();
            byte[] uzenetByteok = utf8encoding.GetBytes(uzenet);
            UInt32 hossz = (UInt32)uzenetByteok.Length;

            BinaryWriter binaryWriter = new BinaryWriter(tcpStream);
            binaryWriter.Write(hossz);

            binaryWriter.Write(uzenetByteok);
        }
    }
}
