﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using karesz;

namespace ConsoleApplication1
{
    public static class GameLogic
    {
        public static bool IsDeadEnd(int PosX, int PosY)
        {
            string idenemszabad = "VWXYT";

            if (idenemszabad.Contains(XMLFileProcessor.Map[PosX + 1, PosY]) ||  //Jobbra?
                idenemszabad.Contains(XMLFileProcessor.Map[PosX - 1, PosY]) ||  //Balra?
                idenemszabad.Contains(XMLFileProcessor.Map[PosX, PosY - 1]) ||  //Felfele?
                idenemszabad.Contains(XMLFileProcessor.Map[PosX, PosY + 1]))    //Balra?
            {
                return false;
            }
            else
                return true;
        }









        static float tdX = -1;
        static float tdY = -1;
        static float tbX = -1;
        static float tbY = -1;
        static int cnt = 0;
        static Stack<MapItem> toProcess = new Stack<MapItem>();
        static MapItem[,] mapItems = new MapItem[XMLFileProcessor.Map.GetLength(0), XMLFileProcessor.Map.GetLength(1)];
        public static int Strategy
        {
            get { return karesz.XMLFileProcessor.Players.Count > 2 ? 1 : 0; }
        }

        public static void CalculateMoves()
        {
            if (Strategy == 0) //1v1 stratégia
            {

            }
            else //free for all stratégia
            {
                int minindex = ClosestPowerUp();
                if (minindex >= 0)
                {
                    Powerup closest = XMLFileProcessor.Powerups[minindex];
                    PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, closest.PosX, closest.PosY);
                }
                else
                {
                    PathFinder(XMLFileProcessor.MyPlayer.PosX, XMLFileProcessor.MyPlayer.PosY, 9, 11);
                }
            }
        }

        static int ClosestPowerUp()
        {
            if (XMLFileProcessor.Powerups.Count > 0)
            {
                int min = 0;
                float minWid = CalculateWidth(XMLFileProcessor.Powerups[0]);
                for (int i = 1; i < XMLFileProcessor.Powerups.Count; i++)
                {
                    float currWid = CalculateWidth(XMLFileProcessor.Powerups[i]);
                    if (currWid < minWid)
                    {
                        minWid = currWid;
                        min = i;
                    }
                }
                return min;
            }
            return -1;
        }
        static float CalculateWidth(Powerup powerup)
        {
            float res = 0;
            res += Math.Abs(powerup.PosX - XMLFileProcessor.MyPlayer.PosX);
            res += Math.Abs(powerup.PosY - XMLFileProcessor.MyPlayer.PosY);
            return res;
        }
        public static bool Danger(int x, int y) //használhatjuk a jelenlegi pozíciónk ellenőrzésére, vagy úticél ellenőrzésre is a paraméterekkel
        {
            foreach (Flame item in XMLFileProcessor.Flames)
            {
                if ((int)(item.PosX) == x && (int)(item.PosY) == y)
                {
                    return true;
                }
            }
            foreach (Bomb item in XMLFileProcessor.Bombs)
            {
                if ((int)(item.PosX + 8) >= x && (int)(item.PosX - 8) <= x && (int)(item.PosY) == y)
                {
                    return true;
                }
                if ((int)(item.PosY + 8) >= y && (int)(item.PosY - 8) <= y && (int)(item.PosX) == x)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool PathFinder(float srcX, float srcY, float dstX, float dstY)
        {
            cnt++;
            float speed = XMLFileProcessor.MyPlayer.WalkSpeed;
            if ((Math.Abs(srcX - tdX) < 1.45 * (speed / 0.25) && Math.Abs(srcY - tdY) < 1.45 * (speed / 0.25)) || cnt > 30)
            {
                Commands.Stop();
                tdX = tdY = -1;
            }
            if (srcX != dstX || srcY != dstY)
            {
                for (int i = 0; i < mapItems.GetLength(0); i++)
                {
                    for (int j = 0; j < mapItems.GetLength(1); j++)
                    {
                        char c = XMLFileProcessor.Map[i, j];
                        if ((c <= '9' && c >= '0') || (c <= 'H' && c >= 'A') || (c <= 'S' && c >= 'O') || c == 'U')
                        {
                            c = 'Z';
                        }
                        mapItems[i, j] = new MapItem(i, j, c);
                    }
                }
                int sourceX = (int)srcX;
                int sourceY = (int)srcY;
                int destinationX = (int)dstX;
                int destinationY = (int)dstY;
                mapItems[destinationX, destinationY].Value = 0;
                ProcessTile(destinationX, destinationY);
                while (toProcess.Count > 0)
                {
                    MapItem actualItem = toProcess.Pop();
                    ProcessTile(actualItem.X, actualItem.Y);
                }
                if (tdX == -1)
                {
                    if (mapItems[sourceX, sourceY].Value - mapItems[sourceX - 1, sourceY].Value == 1)
                    {
                        Commands.Stop();
                        int i = 0;
                        while (sourceX - i - 1 >= 0 && mapItems[sourceX - i, sourceY].Value - mapItems[sourceX - i - 1, sourceY].Value == 1)
                        {
                            i++;
                        }
                        tdX = sourceX - i;
                        tdY = sourceY;
                        Commands.Left();
                        return true;
                    }
                    else if (mapItems[sourceX, sourceY].Value - mapItems[sourceX + 1, sourceY].Value == 1)
                    {
                        Commands.Stop();
                        int i = 0;
                        while (sourceX + i + 1 < mapItems.GetLength(0) && mapItems[sourceX + i, sourceY].Value - mapItems[sourceX + i + 1, sourceY].Value == 1)
                        {
                            i++;
                        }
                        tdX = sourceX + i;
                        tdY = sourceY;
                        Commands.Right();
                        return true;
                    }
                    else if (mapItems[sourceX, sourceY].Value - mapItems[sourceX, sourceY + 1].Value == 1)
                    {
                        Commands.Stop();
                        int i = 0;
                        while (sourceY + i + 1 < mapItems.GetLength(1) && mapItems[sourceX, sourceY + i].Value - mapItems[sourceX, sourceY + i + 1].Value == 1)
                        {
                            i++;
                        }
                        tdY = sourceY + i;
                        tdX = sourceX;
                        Commands.Down();
                        return true;
                    }
                    else if (mapItems[sourceX, sourceY].Value - mapItems[sourceX, sourceY - 1].Value == 1)
                    {
                        Commands.Stop();
                        int i = 0;
                        while (sourceY - i - 1 >= 0 && mapItems[sourceX, sourceY - i].Value - mapItems[sourceX, sourceY - i - 1].Value == 1)
                        {
                            i++;
                        }
                        tdY = sourceY - i;
                        tdX = sourceX;
                        Commands.Up();
                        return true;
                    }
                }
                if (tdX < sourceX)
                    Commands.Left();
                else if (tdX > sourceX)
                    Commands.Right();
                else if (tdY < sourceY)
                    Commands.Up();
                else if (tdY > sourceY)
                    Commands.Down();
            }
            return false;
        }

        static void ProcessTile(int x, int y)
        {
            if (x + 1 < mapItems.GetLength(0) && mapItems[x + 1, y].Type == 'Z' && mapItems[x + 1, y].Value > mapItems[x, y].Value)
            {
                mapItems[x + 1, y].Value = mapItems[x, y].Value + 1;
                toProcess.Push(mapItems[x + 1, y]);
            }
            if (y + 1 < mapItems.GetLength(1) && mapItems[x, y + 1].Type == 'Z' && mapItems[x, y + 1].Value > mapItems[x, y].Value)
            {
                mapItems[x, y + 1].Value = mapItems[x, y].Value + 1;
                toProcess.Push(mapItems[x, y + 1]);
            }
            if (x - 1 >= 0 && mapItems[x - 1, y].Type == 'Z' && mapItems[x - 1, y].Value > mapItems[x, y].Value)
            {
                mapItems[x - 1, y].Value = mapItems[x, y].Value + 1;
                toProcess.Push(mapItems[x - 1, y]);
            }
            if (y - 1 >= 0 && mapItems[x, y - 1].Type == 'Z' && mapItems[x, y - 1].Value > mapItems[x, y].Value)
            {
                mapItems[x, y - 1].Value = mapItems[x, y].Value + 1;
                toProcess.Push(mapItems[x, y - 1]);
            }
        }
    }
}
