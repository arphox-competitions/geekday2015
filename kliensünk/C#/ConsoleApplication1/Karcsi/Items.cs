﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace karesz
{
    public class Player
    {
        public int ID { get; private set; }                     // GameItem identifier
        public string NickName { get; private set; }            // Nickname
        public char TileChar { get; private set; }               // Tile character 
        public bool IsPickable { get; private set; }            // Can you pick it up? 
        public bool IsExplodable { get; private set; }          // Can it be exploded? 
        public bool IsSolid { get; private set; }               // is solid (false = we can walk through it, non-solid) 
        public float PosX { get; private set; }                 // Position X
        public float PosY { get; private set; }                 // Position Y 
        public float WalkSpeed { get; private set; }            // Base walk speed 
        public int BombsAllowed { get; private set; }           // Number of bombs allowed 
        public int BombSize { get; private set; }               // Size of explosions
        public int BombsDetonated { get; private set; }         // Number of bombs detonated 
        public int SecretsCollected { get; private set; }       // Number of powerups collected 
        public int NoClipStepsRemaining { get; private set; }   // Number of movements remaining from the "no clip" powerup 
        public int ArmorStepsRemaining { get; private set; }    // Number of movements remaining from the "armor" powerup 
        public int SpeedUpStepsRemaining { get; private set; }  // Number of movements remaining from the "speed up" powerup 
        public int NoBombStepsRemaining { get; private set; }   // Number of movements remaining from the "no bomb" powerup 
        public int SpeedDownStepsRemaining { get; private set; }// Number of movements remaining from the "speed down" powerup 
        public int AlwaysBombStepsRemaining { get; private set; }// Number of movements remaining from the "always bombs" bonus 
        public bool MustStop { get; private set; }              // if true, player will stop at the next tile 
        public string State { get; private set; }               // Player location: Standing, BetweenTiles, InTile 
        public int BombsLeft { get; private set; }              // Number of bombs left to put down 
        public int DX { get; private set; }                     // X speed 
        public int DY { get; private set; }                     // Y speed 
        public int Died { get; private set; }                   // Number of times the player died 
        public int Kills { get; private set; }                  // Number of times the player killed another player 

        public Player(XElement element)
        {
            State = element.Element("State").Value;
            NickName = element.Element("NickName").Value;

            IsSolid = bool.Parse(element.Element("IsSolid").Value);
            MustStop = bool.Parse(element.Element("MustStop").Value);
            IsPickable = bool.Parse(element.Element("IsPickable").Value);
            IsExplodable = bool.Parse(element.Element("IsExplodable").Value);

            PosX = float.Parse(element.Element("PosX").Value, CultureInfo.InvariantCulture);
            PosY = float.Parse(element.Element("PosY").Value, CultureInfo.InvariantCulture);
            WalkSpeed = float.Parse(element.Element("WalkSpeed").Value, CultureInfo.InvariantCulture);

            DX = int.Parse(element.Element("DX").Value);
            DY = int.Parse(element.Element("DY").Value);
            ID = int.Parse(element.Element("ID").Value);
            Died = int.Parse(element.Element("Died").Value);
            Kills = int.Parse(element.Element("Kills").Value);
            TileChar = Convert.ToChar(int.Parse(element.Element("TileChar").Value));
            BombSize = int.Parse(element.Element("BombSize").Value);
            BombsLeft = int.Parse(element.Element("BombsLeft").Value);
            BombsAllowed = int.Parse(element.Element("BombsAllowed").Value);
            BombsDetonated = int.Parse(element.Element("BombsDetonated").Value);
            SecretsCollected = int.Parse(element.Element("SecretsCollected").Value);
            ArmorStepsRemaining = int.Parse(element.Element("ArmorStepsRemaining").Value);
            NoClipStepsRemaining = int.Parse(element.Element("NoClipStepsRemaining").Value);
            NoBombStepsRemaining = int.Parse(element.Element("NoBombStepsRemaining").Value);
            SpeedUpStepsRemaining = int.Parse(element.Element("SpeedUpStepsRemaining").Value);
            SpeedDownStepsRemaining = int.Parse(element.Element("SpeedDownStepsRemaining").Value);
            AlwaysBombStepsRemaining = int.Parse(element.Element("AlwaysBombStepsRemaining").Value);
        }

        public override string ToString()
        {
            return String.Format("{0}({1})", NickName, ID);
        }
    }

    public class GameItem
    {
        public int ID { get; private set; }             // GameItem identifier
        public char TileChar { get; private set; }      // Tile character
        public bool IsPickable { get; private set; }    // Can you pick it up?
        public bool IsExplodable { get; private set; }  // Can it be exploded?
        public bool IsSolid { get; private set; }       // is solid (true = we can NOT walk through it)
        public int PosX { get; private set; }         // Position X
        public int PosY { get; private set; }         // Position Y

        public GameItem(XElement element)
        {
            ID = int.Parse(element.Element("ID").Value);
            TileChar = Convert.ToChar(int.Parse(element.Element("TileChar").Value));
            IsPickable = bool.Parse(element.Element("IsPickable").Value);
            IsExplodable = bool.Parse(element.Element("IsExplodable").Value);
            IsSolid = bool.Parse(element.Element("IsSolid").Value);
            PosX = int.Parse(element.Element("PosX").Value);
            PosY = int.Parse(element.Element("PosY").Value);
        }

        public override string ToString()
        {
            return String.Format("{0}: [{1};{2}] ({3})", TileChar, PosX, PosY, ID);
        }
    }

    //Special Items:
    public class Bomb
    {
        public int ID { get; private set; }             // GameItem identifier
        public int PosX { get; private set; }         // Position X
        public int PosY { get; private set; }         // Position Y
        public int TimeLeft { get; private set; }       // Time left until detonation
        public int PlayerID { get; private set; }       // The owner Player's ID

        public Bomb(XElement element)
        {
            ID = int.Parse(element.Element("ID").Value);
            PosX = int.Parse(element.Element("PosX").Value);
            PosY = int.Parse(element.Element("PosY").Value);
            TimeLeft = int.Parse(element.Element("TimeLeft").Value);
            PlayerID = int.Parse(element.Element("Owner").Element("ID").Value);
        }

        public override string ToString()
        {
            return String.Format("TIME={0} [{1};{2}] P:{3}", TimeLeft, PosX, PosY, PlayerID);
        }
    }
    public class Flame
    {
        public int ID { get; private set; }             // GameItem identifier
        public int PosX { get; private set; }         // Position X
        public int PosY { get; private set; }         // Position Y
        public int PlayerID { get; private set; }       // The owner Player's ID

        public Flame(XElement element)
        {
            ID = int.Parse(element.Element("ID").Value);
            PosX = int.Parse(element.Element("PosX").Value);
            PosY = int.Parse(element.Element("PosY").Value);
            PlayerID = int.Parse(element.Element("Owner").Element("ID").Value);
        }

        public override string ToString()
        {
            return String.Format("[{1};{2}] P:{3}", PosX, PosY, PlayerID);
        }
    }
    public class Powerup
    {
        public int ID { get; private set; }             // GameItem identifier
        public int PosX { get; private set; }         // Position X
        public int PosY { get; private set; }         // Position Y
        public char TileChar { get; private set; }

        public PowerupType Type { get; private set; }

        public Powerup(XElement element)
        {
            ID = int.Parse(element.Element("ID").Value);
            PosX = int.Parse(element.Element("PosX").Value);
            PosY = int.Parse(element.Element("PosY").Value);
            TileChar = Convert.ToChar(int.Parse(element.Element("TileChar").Value));

            switch(TileChar)
            {
                case 'P': Type = PowerupType.NoClip; break;
                case 'Q': Type = PowerupType.Armor; break;
                case 'R': Type = PowerupType.SpeedUp; break;
                case 'S': Type = PowerupType.NoBomb; break;
                case 'T': Type = PowerupType.SpeedDown; break;
                case 'U': Type = PowerupType.AlwaysBombs; break;
            }
        }

        public override string ToString()
        {
            return String.Format("{0}({3}) [{1};{2}]", Type, PosX, PosY, ID);
        }
    }
    public enum PowerupType
    {
        NoClip, Armor, SpeedUp, NoBomb, SpeedDown, AlwaysBombs

        //P = "no clip": player can go through bombs and wall[effective for 2 movements through walls / bombs]
        //Q = "armor": player will not lose a life[effective for 20 movements]
        //R = "speed up": player will move faster[effective for 20 movements]
        //S = "no bomb": player cannot place bombs[effective for 20 movements]
        //T = "speed down": player will move slower[effective for 20 movements]
        //U = "always bombs": player will always place bombs[effective for 20 movements]
    }
}